package steps;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import pages.HomePage;
import pages.LoginPage;
import pages.MemberHomePage;

public class LoginSteps extends BaseSteps{
	private HomePage homePage;
	private LoginPage loginPage;
	private MemberHomePage memberHomePage;
	
	@Given("I launch the browser and navigate to login page")
	public void openLoginPage() {
		driver.get(System.getProperty("url.anzrewards.stage"));
		homePage = new HomePage(driver);
		loginPage = homePage.clickLogin();
	}
	
	@When("I enter $username and $password in login page")
	public void enterCredentials(String username, String password) {
		memberHomePage = loginPage
							.enterUsername(username)
							.enterPassword(password)
							.clickContinue();
	}
	
	@Then("Home page should be displayed with $text")
	public void validateLogin(String text) {
		if("Successful".equals(text)) {
			Assert.assertTrue(memberHomePage.isDetailsDropdownShown());
			memberHomePage.logout();
		} else {
			Assert.assertFalse(memberHomePage.isDetailsDropdownShown());
		}
	}
}
