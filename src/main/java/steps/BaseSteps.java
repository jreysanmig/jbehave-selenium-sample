package steps;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.jbehave.core.annotations.AfterStory;
import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.BeforeStory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseSteps {
	
	protected WebDriver driver;
	
	@BeforeStory
	public void beforeStory() {
		Properties props = System.getProperties();
		try {
			props.load(new FileInputStream("src/main/resources/test.properties"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.setProperties(props);
		
		System.setProperty("webdriver.chrome.driver", System.getenv("CHROME_DRIVER"));
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	}
	
	@BeforeScenario
	public void beforeScenario() {
		driver.manage().deleteAllCookies();
	}
	
	@AfterStory
	public void afterStory() {
		driver.quit();
	}
	
}
