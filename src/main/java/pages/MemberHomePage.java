package pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MemberHomePage extends BasePage{
	
	@FindBy(css = "ul.nav li.mydetails")
	private WebElement detailsDrp;

	@FindBy(linkText = "Logout")
	private WebElement logoutMenu;

	public MemberHomePage(WebDriver driver) {
		super(driver);
	}
	
	public boolean isDetailsDropdownShown() {
		try {
			return detailsDrp.isDisplayed();
		} catch(NoSuchElementException e) {
			return false;
		}
	}
	
	public MemberHomePage logout() {
		detailsDrp.click();
		logoutMenu.click();
		return this;
	}
	
}
