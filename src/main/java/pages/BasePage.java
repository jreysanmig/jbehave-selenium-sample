package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BasePage extends PageFactory{
	protected WebDriver driver;
	
	@FindBy(css = "a.white-btn")
	private WebElement loginBtn;
	
	public BasePage(WebDriver driver) {
		this.driver = driver;
		initElements(driver, this);
	}
	
	public LoginPage clickLogin() {
		loginBtn.click();
		return new LoginPage(this.driver);
	}
	
}
