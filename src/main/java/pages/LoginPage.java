package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {
	
	@FindBy(id = "ctl01_itxbEmail")
	private WebElement usernameTxt;
	
	@FindBy(id = "ctl01_itxbPassword")
	private WebElement passwordTxt;
	
	@FindBy(id = "ctl01_ctl01_ibtnSubmitLoginButton")
	private WebElement continueBtn;
	
	public LoginPage(WebDriver driver) {
		super(driver);
	}
	
	public LoginPage enterUsername(String username) {
		usernameTxt.sendKeys(username);
		return this;
	}
	
	public LoginPage enterPassword(String password) {
		passwordTxt.sendKeys(password);
		return this;
	}
	
	public MemberHomePage clickContinue() {
		continueBtn.click();
		return new MemberHomePage(driver);
	}
	
}
