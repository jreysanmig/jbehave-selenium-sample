
Scenario: Login

Given I launch the browser and navigate to login page
When I enter <username> and <password> in login page
Then Home page should be displayed with <text>

Examples:
|username|password|text|
|pp_uat44@mastercard.com|Tests0000|Successful|
|pp_uat44@mastercard.com|wrongpw|Failed|
|invaliduser|validpw|Failed|


